# Fighting Fantasy

This set of Powershell scripts is intended to act as an alternative to the Fighting Fantasy Adventure Sheet.  

Note - this project is NOT an adventure game/book itself.  It is merely a companion to be used while playing/reading a Fighting Fantasy adventure book.


## Setup
Note - this project is best run in a standard Powershell window, not the ISE.

Clone the scripts:
git clone https://mattleventhal@bitbucket.org/mattleventhal/fighting-fantasy-adventure-sheet.git

Open a standard Powershell window (not ISE) and navigate into the fighting-fantasy-adventure-sheet folder.

Run ./main.ps1 to start using the adventure sheet.

### Settings
The default settings (found in settings.ps1) copy the starting values of the City of Thieves adventure book.
Edit settings.ps1 as desired, although no changes should be required.


## Using the Adventure Sheet
### First Use
On first use, or when choosing not to load 
a previously saved game, your character attributes will be randomly generated following the same formula as the City of Thieves adventure book, as follows:

Skill - a single dice roll (randomly generated number between 1 and 6) plus 6

Stamina - a double dice roll plus 12

Luck - a single dice roll plus 6

Provisions - 10

Coins - 0

Note: You can change any of these values in the settings.ps1 file.

### Playing The Game
Use the Powershell adventure sheet just like a paper adventure sheet, while reading a Fighting Fantasy adventure book, as follows:

#### Fights
F - Fight.  You will be prompted to enter your opponent's skill and stamina values.  The script will then roll dice (generate random numbers) for both your opponent and your character, deducting stamina from the loser of each round as appropriate until death.  If you lose the fight and die the script currently exits and will need to be re-started (./main.ps1) if you wish to try again.

#### Luck Roll
L - Luck Roll.  A double dice roll is performed, the result compared to your current luck score and a verdict of Lucky or Unlucky returned.  One luck point is then deducted from your total luck score.

#### Coins
A - Add coins

S - Spend (or lose) coins

Simply enter the number of coins gained or lost to adjust your total.

#### Eat Provisions
E - Eat.  Adds 4 stamina points and deducts 1 Provision.


### Saving / Loading Games
Sa - Save

Lo - Load

#### Saving
At any point you can Save your current character attributes / points in order to continue later.  

Enter a name to save the game as (this saves a .ps1 file in the saved_games folder).  

You will also be prompted to enter an optional note, which will be displayed when loading the game later.  This is intended to be a reminder of the page / section you are currently at in the book, and/or any current strategies/thoughts that you are pursuing in the game.

#### Loading
Once a game is saved a list of Saved Games will be presented when running main.ps1 each time.  (This is just a directory listing of the saved_games folder - simply delete any files from that folder if you want to remove them from the Saved Games list).