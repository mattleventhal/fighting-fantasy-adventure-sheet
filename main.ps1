﻿# Main
Clear-Host
Write-Host -ForegroundColor Green "FIGHTING FANTASY`n================`n"
# import settings
. .\settings.ps1

# check for saved games
. .\game_scripts\loadGame.ps1