# Main Menu

# Load stats table
. .\game_scripts\statsTable.ps1

Write-Host "`nMain Menu`n---------"
Write-Host "(F)ight"
Write-Host "(L)uck roll"
Write-Host "(A)dd coins"
Write-Host "(S)pend coins"
Write-Host "(E)at provisions"
Write-Host "(D)rink potion"
Write-Host "(Lo)ad game"
Write-Host "(Sa)ve"
Write-Host "(Q)uit"

$action = Read-Host "`nPerform an action"

switch ($action) {

    f { # FIGHT
        . .\game_scripts\fight.ps1
    }

    l { # LUCK ROLL
        . .\game_scripts\luckRoll.ps1
    }

    a { # ADD COINS
        . .\game_scripts\addCoins.ps1
    }

    s { # SPEND COINS
        . .\game_scripts\spendCoins.ps1
    }

    e { # EAT PROVISIONS
        . .\game_scripts\eatProvisions.ps1
    }

    d { # DRINK POTION
        . .\game_scripts\drinkPotion.ps1
    }

    sa { # SAVE
        . .\game_scripts\save.ps1
    }

    lo { # LOAD GAME
        . .\game_scripts\loadGame.ps1
    }

    q { # QUIT
        $save = Read-Host "Save before quitting?? (y/n)"
        if ($save -eq "y") {
            . .\game_scripts\save.ps1
        }

        Write-Host "`nByebye :("
        exit
    }

    default { # Wrong key
        . .\game_scripts\mainMenu.ps1

    }
}