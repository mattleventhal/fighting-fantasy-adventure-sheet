﻿$savedGames = Get-ChildItem .\saved_games
if ($savedGames) {

    Write-Host "`nSAVED GAMES`n==========="
    $friendlyNames = ($savedGames | select -ExpandProperty name).split(".") | where {$_ -ne "ps1"}
    $friendlyNames | sort | fl | Out-Host
    
    if ($action -ne "lo") {
        $continue = Read-Host "`nContinue saved game? (y/n)"
    }

    if ($continue -eq "y" -or $action -eq "lo") {
        # Select game to continue
        $game = read-host "`nGame to load"

        $game = $game + ".ps1"

        if ($game -in $savedGames.name) {
            . .\saved_games\$game
            [System.Collections.ArrayList]$potions = $potions | ConvertFrom-Json

            if ($note) {
                Write-Host "`nNote to self: $note"
                Pause
            }

        } else {
            Write-Host -ForegroundColor Red "$game not found"
            pause
        }

    } else {
        # Start new game
    . .\game_scripts\startNewGame.ps1

    }

} else {
    
    # Start new game
    . .\game_scripts\startNewGame.ps1
}

# Draw main menu
. .\game_scripts\mainMenu.ps1
