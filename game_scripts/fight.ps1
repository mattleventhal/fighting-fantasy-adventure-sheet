﻿# FIGHT

. .\game_scripts\statsTable.ps1

Start-Sleep -Seconds 1
Write-Host -ForegroundColor Red "`n!!FIGHT!!`n"

Start-Sleep -Seconds 1
$oppSkill = Read-Host "Opponent's skill"
$oppStamina = Read-Host "Opponent's stamina"

Start-Sleep -Seconds 1
Write-Host -ForegroundColor Green "`nGood luck!"
Start-Sleep -Seconds 1

$fighting = 1

while ($fighting) {

    . .\game_scripts\statsTable.ps1

    Write-Host -ForegroundColor Green "`n      Your stamina: $stamina"
    Write-Host -ForegroundColor Red   "Opponent's stamina: $oppStamina`n"

    Start-Sleep -Seconds 1
    $oRoll = Get-Random -Minimum 2 -Maximum 13
    Write-Host -ForegroundColor Red "    Oppoent's roll: $oRoll"

    $oAttack = $oRoll + $oppSkill
    Start-Sleep -Milliseconds 500 
    Write-Host -ForegroundColor Red "  Oppoent's attack: $oAttack"

    Start-Sleep -Seconds 1
    $yRoll = Get-Random -Minimum 2 -Maximum 13
    Write-Host -ForegroundColor Green "`n         Your roll: $yRoll"

    $yAttack = $yRoll + $skill
    Start-Sleep -Milliseconds 500 
    Write-Host -ForegroundColor Green "       Your attack: $yAttack`n"

    Start-Sleep -Milliseconds 500

    if ($oAttack -gt $yAttack) {
        # You lose
        Write-Host -ForegroundColor Red "                $oAttack > $yAttack"
        Write-Host -ForegroundColor Red "                Ahh, you got hit!"
        $stamina -= 2

        if ($luck -gt 0) {
            Write-Host "`n                Attempt a luck roll to reduce the damage?`n                (If you are unlucky the damage will be worse!)"
            $roll = Read-Host "`n                (Y)es, atempt a luck roll.  Press Enter to continue"
            if ($roll -like "y") {
                . .\game_scripts\luckRoll.ps1

                if ($lucky) {
                    Write-Host -ForegroundColor Green "`n                Excellent, damage reduced by only 1`n"
                    $stamina += 1
                    Pause

                } else {
                    Write-Host -ForegroundColor Red "`n                Oh no, damage increased!!`n"
                    $stamina -= 1
                    Pause
                }
            }
        }

        if ($stamina -le 0) {
            Write-Host -ForegroundColor Black -BackgroundColor Red "`n                ARGHHHH!  YOU DIED!!                "
            exit

        } else {
            Start-Sleep -Seconds 1
        }


    } elseif ($yAttack -gt $oAttack) {
        # You win
        Write-Host -ForegroundColor Green "                $oAttack < $yAttack"
        $oppStamina -= 2
        if ($oppStamina -le 0) {
            Write-Host -ForegroundColor Black -BackgroundColor Green "                YOU WIN!"
            $fighting = 0
            Pause
        } else {
            Write-Host -ForegroundColor Green "                Nice hit!                "
            Start-Sleep -Seconds 1

            if ($luck -gt 0) {
                Write-Host "`n                Attempt a luck roll to increase the damage?`n                (If you are unlucky the damage will be less!)"
                $roll = Read-Host "`n                (Y)es, atempt a luck roll.  Press Enter to continue"
                if ($roll -like "y") {
                    . .\game_scripts\luckRoll.ps1
    
                    if ($lucky) {
                        Write-Host -ForegroundColor Green "                Excellent, damage inceased"
                        $oppStamina -= 1
                        Pause

                    } else {
                        Write-Host -ForegroundColor Red "                Oh no, damage decreased!!"
                        $oppStamina += 1
                        Pause
                    }
                }
            }
        }

    } else {
        # Draw
        Write-Host "                $oAttack = $yAttack"
        Write-Host "                Draw."
        Start-Sleep -Seconds 1
    }


}

# back to main menu
. .\game_scripts\mainMenu.ps1