﻿# SAVE

. .\game_scripts\statsTable.ps1

if (!(Test-Path ./saved_games)){
    New-Item -ItemType directory -Path ./saved_games | Out-Null
} else {
    Write-Host "`nSAVE GAME"
    $games = (Get-ChildItem .\saved_games | select -ExpandProperty name).split(".")[0]
}

if ($games) {
    Write-Host "`nSAVED GAMES`n-----------"
    $games | Out-Host
}

$save = read-host "`nSave game as (leave blank to cancel)"

if ($save) {
    $note = Read-Host "Optional note, to remind you of your current page etc"
    Set-Content -Path .\saved_games\$save.ps1 -Value "`$initialSkill = $initialSkill
`$initialStamina = $initialStamina
`$initialLuck = $initialLuck
`$skill = $skill
`$stamina = $stamina
`$luck = $luck
`$provisions = $provisions
`$coins = $coins
`$potions = `'$($potions | convertto-json)`'
`$note = `'$note`'
"

    Write-Host -ForegroundColor Green "`nSaved :)"
    if ($action -eq "q") { exit }
    Pause
}

. .\game_scripts\mainMenu.ps1
