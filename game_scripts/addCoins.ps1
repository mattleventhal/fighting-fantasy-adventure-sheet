﻿# ADD COINS

. .\game_scripts\statsTable.ps1

Start-Sleep -Seconds 1
$add = Read-Host "`nHow many coins did you just get!?"

$coins += $add

Write-Host -ForegroundColor Green "`nGreat, that brings your total coins upto $coins!!`n"

Pause
. .\game_scripts\mainMenu.ps1