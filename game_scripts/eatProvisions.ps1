﻿# EAT PROVISIONS

. .\game_scripts\statsTable.ps1

if ($provisions -gt 0) {

    Start-Sleep -Seconds 1
    Write-Host -ForegroundColor Green "`nOhm nom nom...`n"
    $provisions -= 1
    $stamina += 4

    Start-Sleep -Seconds 1
    Write-Host -ForegroundColor Magenta "Provisions remaining: $provisions"
    
    Start-Sleep -Seconds 1
    Write-Host -ForegroundColor Magenta "Stamina increased to: $stamina`n"

} else {
    Write-Host -ForegroundColor Red "You don't have any provisions left :(`n"
}

Start-Sleep -Seconds 1
Pause

. .\game_scripts\mainMenu.ps1