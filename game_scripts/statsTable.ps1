﻿# Stats table

Clear-Host
Write-Host -ForegroundColor Green "FIGHTING FANTASY`n================"

$global:digits = 0
function get-digits ($i) {
    $global:digits += ($i.ToString()).length
}

get-digits $skill
get-digits $stamina
get-digits $luck
get-digits $provisions
get-digits $coins

$border = "+------------------------------------------------------------"

foreach ($x in 1..$digits) {
    $border += "-"
}

$border += "+"

Write-Host -ForegroundColor Magenta $border
Write-Host -ForegroundColor Magenta "| Skill: $skill  | Stamina: $stamina  | Luck: $luck  | Provisions: $provisions  | Coins: $coins  |"
Write-Host -ForegroundColor Magenta $border