﻿# LUCK ROLL

. .\game_scripts\statsTable.ps1

Start-Sleep -Seconds 1
Write-Host "`nLuck roll"

Start-Sleep -Milliseconds 500
Write-Host -ForegroundColor Magenta "`nRolling...`n"

$i = 0
while ($i -lt 30) {
    . .\game_scripts\statsTable.ps1
    Write-Host "`nLuck roll"
    Write-Host -ForegroundColor Magenta "`nRolling...`n"

    Get-Random -Minimum 2 -Maximum 13
    Start-Sleep -Milliseconds 150

    $i++
}

. .\game_scripts\statsTable.ps1
Write-Host "`nLuck roll"
Write-Host -ForegroundColor Magenta "`nRolling...`n"

$roll = Get-Random -Minimum 2 -Maximum 13

Write-Host $roll

if ($roll -eq $luck) {
    Write-Host -ForegroundColor Green "`nPHEW!  That WAS lucky !!`n"
    $lucky = 1
}

elseif ($roll -lt $luck) {
    Write-Host -ForegroundColor Green "`nYes!  You got lucky :)`n"
    $lucky = 1
}

else {
    Write-Host -ForegroundColor Red "`nOh dear, no luck this time :(`n"
    $lucky = 0
}

$luck -= 1

if (!($fighting)) {
    Pause
    . .\game_scripts\mainMenu.ps1
}
