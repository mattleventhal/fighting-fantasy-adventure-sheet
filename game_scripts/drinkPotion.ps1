# DRINK POTIONS

. .\game_scripts\statsTable.ps1

Start-Sleep -Milliseconds 500

if ($potions) {
    Write-Host "`nPotions`n-------"
    $potions | Out-Host
    
    $drink = (Read-Host "`nWhich potion do you want to drink").ToLower()
    
    if ($drink -in $potions) {
    
        switch ($drink) {
            "skill" {
                $skill = $initialSkill
                Write-Host -ForegroundColor Green "`nGlug, glug, glug...`nAhhh, Skill restored to $skill`n"
                $potions = $potions.Remove($drink)
            }
    
            "stamina" {
                $stamina = $initialStamina
                Write-Host -ForegroundColor Green "`nGlug, glug, glug...`nAhhh, Stamina restored to $stamina`n"
                $potions = $potions.Remove($drink)
            }
    
            "luck" {
                $luck = $initialLuck
                Write-Host -ForegroundColor Green "`nGlug, glug, glug...`nAhhh, Luck restored to $luck`n"
                $potions = $potions.Remove($drink)
            }
        }
    
    } else {
        Write-Host "`nNice try, but you don't have that potion!!"
    }
    
} else {
    Write-Host "`nYou don't have any potions!`n"
}

Start-Sleep -Milliseconds 500
Pause

. .\game_scripts\mainMenu.ps1
