﻿# Start new game

Clear-Host
Write-Host -ForegroundColor Green "FIGHTING FANTASY`n================`n"

Write-Host -ForegroundColor Green "Initializing new game...`n"


$initialSkill = (Get-Random -Minimum 1 -Maximum 7) + $skillAddOn
$initialStamina = (Get-Random -Minimum 2 -Maximum 13) + $staminaAddOn
$initialLuck = (Get-Random -Minimum 1 -Maximum 7) + $luckAddOn

Start-Sleep -Seconds 1

Write-Host -ForegroundColor Magenta "     Skill: $initialSkill"
$skill = $initialSkill
Start-Sleep -Seconds 1

Write-Host -ForegroundColor Magenta "   Stamina: $initialStamina"
$stamina = $initialStamina
Start-Sleep -Seconds 1

Write-Host -ForegroundColor Magenta "      Luck: $initialLuck"
$luck = $initialLuck
Start-Sleep -Seconds 1

Write-Host -ForegroundColor Magenta "Provisions: $provisions"
Start-Sleep -Seconds 1

$coins = 0
Write-Host -ForegroundColor Magenta "     Coins: $coins`n"
Start-Sleep -Seconds 1

Write-Host -ForegroundColor Yellow "`nPotions`n-------"
Write-Host -ForegroundColor Yellow "  (Sk)ill - restores Skill points to $initialSkill"
Write-Host -ForegroundColor Yellow "(St)amina - restores Stamina points to $initialStamina"
Write-Host -ForegroundColor Yellow "   (L)uck - restores Luck points to $initialLuck"
Write-Host "`nYou can take one potion on your quest, and use it only once"

[System.Collections.ArrayList]$potions = @()
function set-potion ($p){
    switch ($p){
        "sk" {
            $global:potions += "skill"
            Write-Host "`nSkill potion added to your bag`n"
        }
    
        "st" {
            $global:potions += "stamina"   
            Write-Host "`nStamina potion added to your bag`n"
        }

        "l" {
            $global:potions += "luck"
            Write-Host "`nLuck potion added to your bag`n"
        }
    }
}

while (!($potions)){
    $p = Read-Host "Which potion would you like to take?  (sk/st/l)"
    set-potion $p
}

Pause