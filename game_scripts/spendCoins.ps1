﻿# SPEND COINS

. .\game_scripts\statsTable.ps1

Start-Sleep -Seconds 1
$loss = Read-Host "`nHow many coins did you just spend/lose"

if ($coins - $loss -lt 0) {
    Write-Host -ForegroundColor Magenta "`nUh oh - I hope not, you only have $coins left!!"
    Write-Host "No action taken`n"
} else {

    $coins -= $loss
    Write-Host "`nCoins remaining: $coins`n"

}

Pause
. .\game_scripts\mainMenu.ps1